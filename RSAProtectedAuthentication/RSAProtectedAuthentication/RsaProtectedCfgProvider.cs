﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSAProtectedAuthentication
{
    public class RsaProtectedCfgProvider
    {
        public static void ProtectConfiguration(string sectionName)
        {
            // Get the application configuration file.
           var config =ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            // Define the Rsa provider name.
            var provider ="RsaProtectedConfigurationProvider";

            // Get the section to protect.
            var connStrings = config.GetSection((sectionName));

            if (connStrings != null)
            {
                if (!connStrings.SectionInformation.IsProtected)
                {
                    if (!connStrings.ElementInformation.IsLocked)
                    {
                        // Protect the section.
                        connStrings.SectionInformation.ProtectSection(provider);

                        connStrings.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Modified, true);

                        Console.WriteLine("Section {0} is now protected by {1}",
                            connStrings.SectionInformation.Name,
                            connStrings.SectionInformation.ProtectionProvider.Name);


                        ConfigurationManager.RefreshSection(sectionName);
                    }
                    else
                        Console.WriteLine(
                             "Can't protect, section {0} is locked",
                             connStrings.SectionInformation.Name);
                }
                else
                    Console.WriteLine(
                        "Section {0} is already protected by {1}",
                        connStrings.SectionInformation.Name,
                        connStrings.SectionInformation.ProtectionProvider.Name);
            }
            else
                Console.WriteLine("Can't get the section {0}",
                    connStrings.SectionInformation.Name);
        }

        // Unprotect the connectionStrings section.
        public static void UnProtectConfiguration(string sectionName)
        {

            // Get the application configuration file.
            //var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            
            // Get the section to unprotect.
            var connStrings = config.GetSection((sectionName));

            if (connStrings != null)
            {
                if (connStrings.SectionInformation.IsProtected)
                {
                    if (!connStrings.ElementInformation.IsLocked)
                    {
                        // Unprotect the section.
                        connStrings.SectionInformation.UnprotectSection();

                        connStrings.SectionInformation.ForceSave = true;
                        //config.Save(ConfigurationSaveMode.Modified, true);

                        //var allKeys = config.AppSettings.Settings.AllKeys;
                        //var pgpPrivateKey = allKeys.Select(x => x.Contains("PGP_PrivateKey"));

                        //Console.WriteLine("Section {0} is now unprotected.", pgpPrivateKey);

                        //Console.WriteLine("Section {0} is now unprotected.",
                        //    connStrings.SectionInformation.Name);

                        ConfigurationManager.RefreshSection(sectionName);

                        var pgpPrivateKey = ConfigurationManager.AppSettings[0];

                        Console.WriteLine(pgpPrivateKey);
                        //Console.WriteLine(connStrings.SectionInformation.GetRawXml().Contains("PGP_PrivateKey").ToString());
                    }
                    else
                        Console.WriteLine(
                             "Can't unprotect, section {0} is locked",
                             connStrings.SectionInformation.Name);
                }
                else
                    Console.WriteLine(
                        "Section {0} is already unprotected.",
                        connStrings.SectionInformation.Name);
            }
            else
                Console.WriteLine("Can't get the section {0}",
                    connStrings.SectionInformation.Name);
        }

        public static void EncryptAppSettings(string section)
        {
             //Configuration objConfig = ConfigurationManager.OpenExeConfiguration(GetAppPath() + "AppKeyEncryption.exe");
             // Configuration objConfig = ConfigurationManager.OpenExeConfiguration(GetAppPath() + "RSAProtectedAuthentication.exe");
               Configuration objConfig = ConfigurationManager.OpenExeConfiguration(GetAppPath() + "App.config");            
             //Configuration objConfig = ConfigurationManager.OpenExeConfiguration(GetAppPath() + "AppSettingDoc.exe");
             //Configuration objConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

             AppSettingsSection objAppsettings = (AppSettingsSection)objConfig.GetSection(section);
            if (!objAppsettings.SectionInformation.IsProtected)
            {
                objAppsettings.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                objAppsettings.SectionInformation.ForceSave = true;
                //KeyValueConfigurationCollection settings = objConfig.AppSettings.Settings;
                //settings["PGP_PrivateKey"].Value = "";
                objConfig.Save(ConfigurationSaveMode.Modified, true);
            }
        }

        /// <summary>
        /// This method is used to fetch the location of location of executable 
        /// </summary>
        /// <returns>location of executable</returns>
        private static string GetAppPath()
        {
            //System.Reflection.Module[] modules = System.Reflection.Assembly.GetExecutingAssembly().GetModules();
            //string location = System.IO.Path.GetDirectoryName(modules[0].FullyQualifiedName);
            //if ((location != "") && (location[location.Length - 1] != '\\'))
            //    location += '\\';
            var location = @"C:\\Users\\544396\\source\\repos\\AppSettingsFile\\AppSettingsFile\\";
            return location;
        }

    }
}
