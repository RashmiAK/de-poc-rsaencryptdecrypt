﻿using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.Configuration;
using System.IO;
using System.Text;
using Xunit;
using DidiSoft.Pgp;
using DidX.BouncyCastle.Bcpg.OpenPgp;

namespace RSAProtectedAuthentication
{
    class Program
    {
        /// <summary>
        /// This method is 'Main' method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var sectionName = "appSettings";

            RsaProtectedCfgProvider.ProtectConfiguration(sectionName);
                       

            //RsaProtectedCfgProvider.EncryptAppSettings(sectionName);


            RsaProtectedCfgProvider.UnProtectConfiguration(sectionName);

            Console.ReadKey();
        }       

    }

    public class DecryptString
    {
        public String Decrypt(String encryptedString,byte[] privateKeyBytes)
        {
            string birds = encryptedString;

            PGPLib pgp = new PGPLib();
          
            Stream inputStream = new MemoryStream(privateKeyBytes);
            inputStream = PgpUtilities.GetDecoderStream(inputStream);


            String plainString =
              pgp.DecryptString(encryptedString,
                                  inputStream,
                                 //new FileInfo(@"C:\Users\544396\source\repos\AnPostPGPCertificate_priv.asc"),
                                "AnPost");

            return plainString;
        }

        public String Encrypt(String encryptedString)
        {
            // create an instance of the library
            PGPLib pgp = new PGPLib();

            string enString =   pgp.EncryptString(encryptedString, new FileInfo("C:\\Users\\544396\\source\\repos\\AnPostPGPCertificate_public.asc"));
            return enString;
        }
    }
}

