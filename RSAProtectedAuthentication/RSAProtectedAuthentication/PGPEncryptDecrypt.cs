﻿using NUnit.Framework;
using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.IO;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using DidiSoft.Pgp;

namespace RSAProtectedAuthentication
{
    public class Pgp
    {
        /**
        * A simple routine that opens a key ring file and loads the first available key suitable for
        * encryption.
        *
        * @param in
        * @return
        * @m_out
        * @
        */

        //public static void DecryptFile(
        //string inputFileName,
        //string keyFileName,
        //char[] passwd,
        //string defaultFileName)
        //{
        //    using (Stream input = File.OpenRead(inputFileName),
        //           keyIn = File.OpenRead(keyFileName))
        //    {
        //        DecryptFile(input, keyIn, passwd, defaultFileName);
        //    }
        //}





        public static void DecryptFile(
           Stream inputStream,
           Stream keyIn,
           char[] passwd,
           string defaultFileName)
        {
            inputStream = PgpUtilities.GetDecoderStream(inputStream);

            try
            {
                PgpObjectFactory pgpF = new PgpObjectFactory(inputStream);
                PgpEncryptedDataList enc;

                PgpObject o = pgpF.NextPgpObject();
                //
                // the first object might be a PGP marker packet.
                //
                if (o is PgpEncryptedDataList)
                {
                    enc = (PgpEncryptedDataList)o;
                }
                else
                {
                    enc = (PgpEncryptedDataList)pgpF.NextPgpObject();
                }

                //
                // find the secret key
                //
                PgpPrivateKey sKey = null;
                PgpPublicKeyEncryptedData pbe = null;
                PgpSecretKeyRingBundle pgpSec = new PgpSecretKeyRingBundle(
                    PgpUtilities.GetDecoderStream(keyIn));

                foreach (PgpPublicKeyEncryptedData pked in enc.GetEncryptedDataObjects())
                {
                    sKey = FindSecretKey(pgpSec, pked.KeyId, passwd);

                    if (sKey != null)
                    {
                        pbe = pked;
                        break;
                    }
                }

                if (sKey == null)
                {
                    throw new ArgumentException("secret key for message not found.");
                }

                Stream clear = pbe.GetDataStream(sKey);

                PgpObjectFactory plainFact = new PgpObjectFactory(clear);

                PgpObject message = plainFact.NextPgpObject();

                if (message is PgpCompressedData)
                {
                    PgpCompressedData cData = (PgpCompressedData)message;
                    PgpObjectFactory pgpFact = new PgpObjectFactory(cData.GetDataStream());

                    message = pgpFact.NextPgpObject();
                }

                if (message is PgpLiteralData)
                {
                    PgpLiteralData ld = (PgpLiteralData)message;

                    string outFileName = ld.FileName;
                    if (outFileName.Length == 0)
                    {
                        outFileName = defaultFileName;
                    }

                    Stream fOut = File.Create(outFileName);
                    Stream unc = ld.GetInputStream();
                    Streams.PipeAll(unc, fOut);
                    fOut.Close();
                }
                else if (message is PgpOnePassSignatureList)
                {
                    throw new PgpException("encrypted message contains a signed message - not literal data.");
                }
                else
                {
                    throw new PgpException("message is not a simple encrypted file - type unknown.");
                }

                if (pbe.IsIntegrityProtected())
                {
                    if (!pbe.Verify())
                    {
                        Console.Error.WriteLine("message failed integrity check");
                    }
                    else
                    {
                        Console.Error.WriteLine("message integrity check passed");
                    }
                }
                else
                {
                    Console.Error.WriteLine("no message integrity check");
                }
            }
            catch (PgpException e)
            {
                Console.Error.WriteLine(e);

                Exception underlyingException = e.InnerException;
                if (underlyingException != null)
                {
                    Console.Error.WriteLine(underlyingException.Message);
                    Console.Error.WriteLine(underlyingException.StackTrace);
                }
            }
        }



        public static PgpPublicKey ReadPublicKey(Stream inputStream)
        {
                inputStream = PgpUtilities.GetDecoderStream(inputStream);
                PgpPublicKeyRingBundle pgpPub = new PgpPublicKeyRingBundle(inputStream);
                //
                // we just loop through the collection till we find a key suitable for encryption, in the real
                // world you would probably want to be a bit smarter about this.
                //
                //
                // iterate through the key rings.
                //
                foreach (PgpPublicKeyRing kRing in pgpPub.GetKeyRings())
                {
                    foreach (PgpPublicKey k in kRing.GetPublicKeys())
                    {
                        if (k.IsEncryptionKey)
                            return k;
                    }
                }

             throw new ArgumentException("Can't find encryption key in key ring.");
        }
              


        /**
        * Search a secret key ring collection for a secret key corresponding to
        * keyId if it exists.
        *
        * @param pgpSec a secret key ring collection.
        * @param keyId keyId we want.
        * @param pass passphrase to decrypt secret key with.
        * @return
        */
        private static PgpPrivateKey FindSecretKey(PgpSecretKeyRingBundle pgpSec, long keyId, char[] pass)
        {
            PgpSecretKey pgpSecKey = pgpSec.GetSecretKey(keyId);
            if (pgpSecKey == null)
                return null;

            return pgpSecKey.ExtractPrivateKey(pass);
        }

        /**
        * Decrypt the byte array passed into inputData and return it as
        * another byte array.
        *
        * @param inputData - the data to decrypt
        * @param keyIn - a stream from your private keyring file
        * @param passCode - the password
        * @return - decrypted data as byte array
        */
        public static byte[] Decrypt(byte[] inputData, Stream keyIn, string passCode)
        {
            byte[] error = Encoding.ASCII.GetBytes("ERROR");

            Stream inputStream = new MemoryStream(inputData);
            inputStream = PgpUtilities.GetDecoderStream(inputStream);
            MemoryStream decoded = new MemoryStream();

            try
            {
                PgpObjectFactory pgpF = new PgpObjectFactory(inputStream);
                PgpEncryptedDataList enc;
                PgpObject o = pgpF.NextPgpObject();

                //
                //the first object might be a PGP marker packet.


                if (o is PgpEncryptedDataList)
                    enc = (PgpEncryptedDataList)o;
                else
                    enc = (PgpEncryptedDataList)pgpF.NextPgpObject();


                //
                // find the secret key
                //
                PgpPrivateKey sKey = null;
                PgpPublicKeyEncryptedData pbe = null;
                PgpSecretKeyRingBundle pgpSec = new PgpSecretKeyRingBundle(
                PgpUtilities.GetDecoderStream(keyIn));
                foreach (PgpPublicKeyEncryptedData pked in enc.GetEncryptedDataObjects())
                {
                    sKey = FindSecretKey(pgpSec, pked.KeyId, passCode.ToCharArray());
                    if (sKey != null)
                    {
                        pbe = pked;
                        break;
                    }
                }
                if (sKey == null)
                    throw new ArgumentException("secret key for message not found.");

                Stream clear = pbe.GetDataStream(sKey);
                PgpObjectFactory plainFact = new PgpObjectFactory(clear);
                PgpObject message = plainFact.NextPgpObject();

                if (message is PgpCompressedData)
                {
                    PgpCompressedData cData = (PgpCompressedData)message;
                    PgpObjectFactory pgpFact = new PgpObjectFactory(cData.GetDataStream());
                    message = pgpFact.NextPgpObject();
                }
                if (message is PgpLiteralData)
                {
                    PgpLiteralData ld = (PgpLiteralData)message;
                    Stream unc = ld.GetInputStream();
                    PipeAll(unc, decoded);
                }
                else if (message is PgpOnePassSignatureList)
                    throw new PgpException("encrypted message contains a signed message - not literal data.");
                else
                    throw new PgpException("message is not a simple encrypted file - type unknown.");

                if (pbe.IsIntegrityProtected())
                {
                    //if (!pbe.Verify())
                    //    MessageBox.Show(null, "Message failed integrity check.", "PGP Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //else
                    //    MessageBox.Show(null, "Message integrity check passed.", "PGP Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    //MessageBox.Show(null, "No message integrity check.", "PGP Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return decoded.ToArray();
            }
            catch (Exception e)
            {
                //if (e.Message.StartsWith("Checksum mismatch"))
                //    MessageBox.Show(null, "Likely invalid passcode. Possible data corruption.", "Invalid Passcode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //else if (e.Message.StartsWith("Object reference not"))
                //    MessageBox.Show(null, "PGP data does not exist.", "PGP Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //else if (e.Message.StartsWith("Premature end of stream"))
                //    MessageBox.Show(null, "Partial PGP data found.", "PGP Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //else
                //    MessageBox.Show(null, e.Message, "PGP Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Exception underlyingException = e.InnerException;
                //if (underlyingException != null)
                //    MessageBox.Show(null, underlyingException.Message, "PGP Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return error;
            }
        }

        /**
        * Encrypt the data.
        *
        * @param inputData - byte array to encrypt
        * @param passPhrase - the password returned by "ReadPublicKey"
        * @param withIntegrityCheck - check the data for errors
        * @param armor - protect the data streams
        * @return - encrypted byte array
        */
         public static byte[] Encrypt(byte[] inputData, PgpPublicKey passPhrase, bool withIntegrityCheck, bool armor)
         {
            byte[] processedData = Compress(inputData, PgpLiteralData.Console, CompressionAlgorithmTag.Uncompressed);
            
            MemoryStream bOut = new MemoryStream();
            Stream output = bOut;
           
            

            if (armor)
                output = new ArmoredOutputStream(output);


            //PgpEncryptedDataGenerator encGen = new PgpEncryptedDataGenerator(SymmetricKeyAlgorithmTag.Cast5, withIntegrityCheck, new SecureRandom());
            PgpEncryptedDataGenerator encGen = new PgpEncryptedDataGenerator(SymmetricKeyAlgorithmTag.Null);
            encGen.AddMethod(passPhrase);

            Stream encOut = encGen.Open(output, processedData.Length);

            encOut.Write(processedData, 0, processedData.Length);
            encOut.Close();

            if (armor)
                output.Close();

            return bOut.ToArray();
        }

        public byte[] Encrypt(byte[] inputData, byte[] publicKey)
        {
            Stream publicKeyStream = new MemoryStream(publicKey);

            PgpPublicKey encKey = ReadPublicKey(publicKeyStream);

            return Encrypt(inputData, encKey, true, true);
            //return Encrypt(inputData, true);
        }

        private static byte[] Compress(byte[] clearData, string fileName, CompressionAlgorithmTag algorithm)
        {
            MemoryStream bOut = new MemoryStream();

            PgpCompressedDataGenerator comData = new PgpCompressedDataGenerator(algorithm);
            Stream cos = comData.Open(bOut); // open it with the final destination
            PgpLiteralDataGenerator lData = new PgpLiteralDataGenerator();

            // we want to Generate compressed data. This might be a user option later,
            // in which case we would pass in bOut.
            Stream pOut = lData.Open(
            cos,                    // the compressed output stream
            PgpLiteralData.Binary,
            fileName,               // "filename" to store
            clearData.Length,       // length of clear data
            DateTime.UtcNow         // current time
            );

            pOut.Write(clearData, 0, clearData.Length);
            pOut.Close();

            comData.Close();

            return bOut.ToArray();
        }

        private const int BufferSize = 512;

        public static void PipeAll(Stream inStr, Stream outStr)
        {
            byte[] bs = new byte[BufferSize];
            int numRead;
            while ((numRead = inStr.Read(bs, 0, bs.Length)) > 0)
            {
                outStr.Write(bs, 0, numRead);
            }
        }



        public byte[] DecryptMessage(byte[] message)
        {
            char[] pgpPassPhrase = { 'A','n','P','o','s','t' };
            PgpObjectFactory pgpF = new PgpObjectFactory(message);
            PgpEncryptedDataList enc = (PgpEncryptedDataList)pgpF.NextPgpObject();
            PgpPbeEncryptedData pbe = (PgpPbeEncryptedData)enc[0];
            
            Stream clear = pbe.GetDataStream(pgpPassPhrase);

            PgpObjectFactory pgpFact = new PgpObjectFactory(clear);
            PgpCompressedData cData = (PgpCompressedData)pgpFact.NextPgpObject();
            pgpFact = new PgpObjectFactory(cData.GetDataStream());

            PgpLiteralData ld = (PgpLiteralData)pgpFact.NextPgpObject();

            if (!ld.FileName.Equals("test.txt")
                && !ld.FileName.Equals("_CONSOLE"))
            {
                //Fail("wrong filename in packet");
                Console.WriteLine("wrong filename in packet");
            }

            //if (!ld.ModificationTime.Equals(TestDateTime))
            //{
            //    Fail("wrong modification time in packet: " + ld.ModificationTime + " vs " + TestDateTime);
            //}

            Stream unc = ld.GetInputStream();
            byte[] bytes = Pgp.ReadAll(unc);

            if (pbe.IsIntegrityProtected() && !pbe.Verify())
            {
                //Fail("integrity check failed");
            }

            return bytes;
        }

        public static byte[] ReadAll(Stream inStr)
        {
            MemoryStream buf = new MemoryStream();
            PipeAll(inStr, buf);
            return buf.ToArray();
        }      



        /// <summary>

        /// PGP decrypt a given stream.

        /// </summary>

        /// <param name="inputStream">PGP encrypted data stream</param>

        /// <param name="outputStream">Output PGP decrypted stream</param>

        /// <param name="privateKeyStream">PGP secret key stream</param>

        /// <param name="passPhrase">PGP secret key password</param>

        public  Stream DecryptStream(Stream inputStream, Stream outputStream, Stream privateKeyStream, string passPhrase)
        {

            if (inputStream == null)

                throw new ArgumentException("InputStream");

            if (outputStream == null)

                throw new ArgumentException("OutputStream");

            if (privateKeyStream == null)

                throw new ArgumentException("PrivateKeyFilePath");

            if (passPhrase == null)

                passPhrase = String.Empty;


            DecryptAsync(inputStream, outputStream, privateKeyStream, passPhrase);

            return outputStream;

        }



        private void DecryptAsync(Stream inputStream, Stream outputStream, Stream privateKeyStream, string passPhrase)
        {
            if (inputStream == null)
                throw new ArgumentException("InputStream");
            if (outputStream == null)
                throw new ArgumentException("outputStream");
            if (privateKeyStream == null)
                throw new ArgumentException("privateKeyStream");

            if (passPhrase == null)
                passPhrase = String.Empty;


            PgpObjectFactory objFactory = new PgpObjectFactory(PgpUtilities.GetDecoderStream(inputStream));
            // find secret key
            PgpSecretKeyRingBundle pgpSec = new PgpSecretKeyRingBundle(PgpUtilities.GetDecoderStream(privateKeyStream));

            PgpObject obj = null;

            if (objFactory != null)
                obj = objFactory.NextPgpObject();


            // the first object might be a PGP marker packet.

            PgpEncryptedDataList enc = null;

            if (obj is PgpEncryptedDataList)
                enc = (PgpEncryptedDataList)obj;

            else
                enc = (PgpEncryptedDataList)objFactory.NextPgpObject();


            // decrypt

            PgpPrivateKey privateKey = null;

            PgpPublicKeyEncryptedData pbe = null;

            foreach (PgpPublicKeyEncryptedData pked in enc.GetEncryptedDataObjects())
            {

                privateKey = FindSecretKey(pgpSec, pked.KeyId, passPhrase.ToCharArray());

                if (privateKey != null)
                {

                    pbe = pked;
                    break;

                }

            }



            if (privateKey == null)

                throw new ArgumentException("Secret key for message not found.");


            PgpObjectFactory plainFact = null;

            using (Stream clear = pbe.GetDataStream(privateKey))
            {

                plainFact = new PgpObjectFactory(clear);

            }


            PgpObject message = plainFact.NextPgpObject();


            if (message is PgpOnePassSignatureList)
            {
                message = plainFact.NextPgpObject();
            }



            if (message is PgpCompressedData)
            {

                PgpCompressedData cData = (PgpCompressedData)message;

                PgpObjectFactory of = null;



                using (Stream compDataIn = cData.GetDataStream())

                {

                    of = new PgpObjectFactory(compDataIn);

                }



                message = of.NextPgpObject();

                if (message is PgpOnePassSignatureList)

                {

                    message = of.NextPgpObject();

                    PgpLiteralData Ld = null;

                    Ld = (PgpLiteralData)message;

                    Stream unc = Ld.GetInputStream();

                     Streams.PipeAll(unc, outputStream);

                }

                else

                {

                    PgpLiteralData Ld = null;

                    Ld = (PgpLiteralData)message;

                    Stream unc = Ld.GetInputStream();

                     Streams.PipeAll(unc, outputStream);

                }

            }

            else if (message is PgpLiteralData)

            {

                PgpLiteralData ld = (PgpLiteralData)message;

                string outFileName = ld.FileName;



                Stream unc = ld.GetInputStream();

                 Streams.PipeAll(unc, outputStream);



                if (pbe.IsIntegrityProtected())

                {

                    if (!pbe.Verify())

                    {

                        throw new PgpException("Message failed integrity check.");

                    }

                }

            }

            else if (message is PgpOnePassSignatureList)

                throw new PgpException("Encrypted message contains a signed message - not literal data.");

            else

                throw new PgpException("Message is not a simple encrypted file.");

        }


    }

}

