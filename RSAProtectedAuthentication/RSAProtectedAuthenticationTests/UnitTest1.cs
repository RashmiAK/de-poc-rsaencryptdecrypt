using System;
using System.IO;
using System.Text;
using Xunit;
using RSAProtectedAuthentication;
using Org.BouncyCastle.Bcpg.OpenPgp;

namespace RSAProtectedAuthenticationTests
{
    public class UnitTest1
    {
        [Fact]
        public void PgpEncryptDecryptTest()
        {
                       
            //var input = "---- - BEGIN PGP MESSAGE-----\r\nVersion: BCPG v1.58\r\n\r\nhQIMA6QtWIOKon1wAQ//XocHD484b8ktEFS6VfAIKoKTrW3Fqt2wKQKIgfLOGfRj\r\niuDOkU6p2FGT5IJR7Werga2uJWOoVEEcz+T2sTOr6KrwWe1omKm0TtNHNj5zPlFa\r\nssiP1g+DBf9wnjhlxdrc4ORIUz2CxneryzUR932W8tyEjRd9PTbVWpmzVNWrJeqj\r\nctX2OvA42c8m6B5Oe6a3mRck3gxI8IYdiAAMi4eZkrB2YQ29nYvDRIu05meAknfq\r\nW+1cmbLHyTV1iU5C79N4nm6ghiyRu/33Pxr+2s0tWHlxgznulAvPff5uX6Rq9l7V\r\n+Y0zEVQ94/reMXXv6zGOkwdirYbPKCAxlS4mqr+v0h5WxT6cihUqlOKFgEZ0QXYz\r\nMi8RMP0bWE1YsWF1rDypzrnxnLL9xTmGGg/cJdBaWEHzGL47OSJR5/yUws6UO4QF\r\nWVStC6OQqSInT2/l67e88sQiwONAFZZBfyAv9U0pPMZJyKfxd3zW530l36XJ7NwK\r\nE2kOqveoNfANyaHnFDW3+G4FivxpH8GlmqV7f7yuTzsvYBwHAHZ/tag2+KJZ00BT\r\nsFKGpdUVuPgeDk+ky+/vfElVwd6MtS1oo4iUO9SWcvtahY9QMYtP+zyWUzXL1kXU\r\ncY5OMPMmBpQ+MNdxG7dKkDY+jHf0x4PL475PO3cxjEYs4GPDhTO9Ob9IAsh2PcfS\r\nQwEliQkBv2o+u1LW22/BW0gbtYTOdtx5nwEKZtLmmQFWNykvfEduN8THOkou1mAn\r\nXsXd6KtiEopQPia1AxB64OZx/X8=\r\n=n18o\r\n-----END PGP MESSAGE-----\r\n";
            //var inputBytes = Encoding.ASCII.GetBytes(input);
            var input = @"-----BEGIN PGP MESSAGE-----
                            Version: BCPG v1.58


                            hQIMA6QtWIOKon1wAQ//XocHD484b8ktEFS6VfAIKoKTrW3Fqt2wKQKIgfLOGfRj
                            iuDOkU6p2FGT5IJR7Werga2uJWOoVEEcz + T2sTOr6KrwWe1omKm0TtNHNj5zPlFa
                            ssiP1g + DBf9wnjhlxdrc4ORIUz2CxneryzUR932W8tyEjRd9PTbVWpmzVNWrJeqj
                            ctX2OvA42c8m6B5Oe6a3mRck3gxI8IYdiAAMi4eZkrB2YQ29nYvDRIu05meAknfq
                            W + 1cmbLHyTV1iU5C79N4nm6ghiyRu / 33Pxr + 2s0tWHlxgznulAvPff5uX6Rq9l7V
                                  + Y0zEVQ94 / reMXXv6zGOkwdirYbPKCAxlS4mqr + v0h5WxT6cihUqlOKFgEZ0QXYz
                            Mi8RMP0bWE1YsWF1rDypzrnxnLL9xTmGGg / cJdBaWEHzGL47OSJR5 / yUws6UO4QF
                            WVStC6OQqSInT2 / l67e88sQiwONAFZZBfyAv9U0pPMZJyKfxd3zW530l36XJ7NwK
                            E2kOqveoNfANyaHnFDW3 + G4FivxpH8GlmqV7f7yuTzsvYBwHAHZ / tag2 + KJZ00BT
                            sFKGpdUVuPgeDk + ky +/ vfElVwd6MtS1oo4iUO9SWcvtahY9QMYtP + zyWUzXL1kXU
                            cY5OMPMmBpQ + MNdxG7dKkDY + jHf0x4PL475PO3cxjEYs4GPDhTO9Ob9IAsh2PcfS
                            QwEliQkBv2o + u1LW22 / BW0gbtYTOdtx5nwEKZtLmmQFWNykvfEduN8THOkou1mAn
                            XsXd6KtiEopQPia1AxB64OZx / X8 =
                            = n18o
                            ---- - END PGP MESSAGE-----";



            var messageKeyBytes = Encoding.ASCII.GetBytes(input);





            var pgp = new Pgp();
            var decryptString = new DecryptString();
           
            var privateKey = @"-----BEGIN PGP PRIVATE KEY BLOCK-----
                                            Version: BCPG v1.45

                                            lQcsBF8/e9IBEAC5BlBqt9C/7rrJW7P/VYvbccymzGmvuWzDQDbM+6Fmv/6lY+sN
                                            2hQtC+mKdJ1iX79+b0m9goRFh6Z6xGZYM0fpiJf8gaRWW8lliZ0QvdebPMQZrwmH
                                            JmHU3iFRKG8cMcOt+U/7Zr/QsxMaVclTn708GbC7u0dLmufK6nyqPQcf33mhzzs3
                                            SvMDyD3Ohp8Smg762WW3CLpgccpPISBWe/VoVasljK+/Ramgw+fRvYN2kn0I4ArL
                                            9EdLG326cXJknCELoDHYc0Gtr+EeIPoFMWoh178pFaQ6aNo+u5U5iZLPBXWBxUlL
                                            O+9OODYoL2sMTp6C4dY+mCknkY8abFzRYfe462uImX8jvIgUXRZ5gIM4RWkXgCwp
                                            hhAINYPQrv1FVkyKT5pvSo8b/1DAzjKVrjqZWqsQcFREnU6dhnmBXVdFue+uM9iD
                                            qYVXtK683J7020oUg361wdB+mamTZoFGTHAwSWjx4xl80I06M4mSEqjhA61zv9mC
                                            JoubD/9ETyh1dv9ra6URAuaCPcSyw9tQBfUihYWk2iQf16rZN9y2smXVoyv4ygBN
                                            +45FGnFCdP7c1oiwaspV0W8gEgVsD6d5Io7avR7u1d/gTBMK7zWZgEol1s7BUcIY
                                            +wIhyCEbkspktLHRCYIjKiXWW+QNINSgWO9TFYP8Phlo0qqA1xFx7d/TDwARAQAB
                                            /wMDAtSKESOQCILpYCqP3Xgfq1RlmH18gYRssCEwiml4VRsNM9JN/S21r/OLL1SB
                                            bZ4mBJ8Q7yL4hNbyc5lrTC2h5+rRgvcjyMXZpIH8F8PEQixgB8ZE2M7/g4isIbnz
                                            5S1ZOfx4EgBWJiiUT6k7PKhibMoJI+hTIOMS/tLb5120FN9c60GhU8JledGayVjF
                                            IdIXXdXfiC50ePf6eDA62CAk1k0Tmr8xc62miZF3dEGABGe4vMA8ZygpFZbV45w3
                                            Q+uS4w/W7sX7SViybYLQ4RX2r0idB58r+/wkdmHb/6talvhRqY3ctFfaIIkfVtxD
                                            XK0OpZuBNsurDQpPA6u23PMD/E7SF0/iGxbBTgLDIjMc9r8tb420IG9ApYyQoiuC
                                            prtx5PBgzWR6DZxjEqrl3auYPTu7CDszfXju9StO/YO68D8o8V7aofFRVG4uRZJY
                                            8ODYhXUlIuVVBtLOqGL+r+soiVyN7kv1CY3vPjh30YdClnQxQc0TQYTh88xJhfpK
                                            zd1EkvXRPOuVQCUZEX/FZ6vRG6343Ed7QShRls1gzQ8DIlIqVrCpjPhdyd1U9qsf
                                            z18AwW7EBMJAXb2qcWMZB/Jny3YXDkjYdHVnVAUGf2qVZu1SzNtIGQkiYb2LGl/D
                                            qjrguL8zu2KRb42KKjj7pKDHc/k7o+VCmTKVHPlcCNXPlgSRhC1K4ntQHlmgRtzK
                                            ACbmU5EwDRRD5HgBiNE+1KL4CwAC/g3Qg5NPUVpiVR05f2CU5lpmt4YXXtA+CQT1
                                            6kXKZueE1ucby3PoGqcWeFcHGnST0UYN9NLno9hMp08fq7+X1AegGufbro/+JAMX
                                            l0+kcOEsY0vSCVPdgr08klvFN43XKdwUMFVCxRsNM1eHnQ46oEELLPGyl2XgUNWX
                                            xH/JsfPZgojS3pALhgSjO+mZqcQP3jVJulSBb644sxbEhiUZTekEmPiZ7VzQjnmL
                                            kXaqNIVrp3V3z8gPf5sTxM0TLqziuZ6ri3C2taY7horLVR3FbAn6g4VhqiSKKmis
                                            8WkxLit850QVYAvqRH6D9wCpHpZvef9khYPuMtDVGIM2lv8/7bW4AtFjEP0BzvDS
                                            WVL964lQSp/c8mz0NzvuQD+7OmqNLdAMWU8XWryJ8gYehoQTnOc26VArfhU7mchd
                                            4SG32W+2tzFd5+2sVIifZtzaTmsuT4K4CS2zUymH+ui+WtjjlU/qQATWy7xOZJqe
                                            OTCGSSIj0ShuXxzAax+0CieHVJYrQemJWMVsc+biJp0w5zDXrZRRCd6sEMCQF22r
                                            QE8hizZ0wHWzVODjjgWCWKD06/KJN3V+O4p+zjvYHP/ENBR442EVudDRwFpV+fNM
                                            lYDBXEK1SQmngDov8oNn5a2geUl34TqlCnT2uHOAxHVm0YAXSXZicRNLPMC0zZBX
                                            pvdTBtOdk/HWHlmdEHo/tB6z4HHGfPhj3xPKyzZM7PkeXSqy/rmGx2CqKA8oeeFL
                                            iKUqh9z4y7rGcaqRp+ywK8woyxVJUV63KPSSE24iH/fS3O0vKj8SItC44IErkf1f
                                            lcl7WJ9jTHJeqFYg1+RyUKR1f5MxFPKsRTNcNbdmjJ8hPy/lNRdFADu8Pc2RT80w
                                            fKa+5VPAjqD9Elrs93w9v65rxtf2tf6ZCs7RNEJUNO/j8Z58XRac+qPmbEsjQvEU
                                            NzTY/jrgeCVdVgxK24g6UU7EOagjqeHFZQ3i6UbCSoZlnVt55RmcCW3WW6xFh5z5
                                            FWzjUabHCUaeCxRY8Fr8tAZBblBvc3SJAhwEEAECAAYFAl8/e9IACgkQpC1Yg4qi
                                            fXCHSA//fCst0iatH6K0NSHbR0NY7qHQKjVZ8vcA3KUrT/FbthpaN5ujofrDf75D
                                            Ets2FH8qmKAY8l6l7aNlpgSmoLZtsQuAx52gPfHZ/Cs4J88fZECz9k9VhLAaDIXw
                                            vVfMmShTrPTG+M9GfMrDmicfOF5MuO+fPyZUHFbvxMFGmiCZEzcy9ZF+jERMbXr/
                                            InhmVtr3cBDonlUKvSzTA0506laOdKM2JVdOua8nJ1fufy6WK61TORLbaKDhRtBL
                                            /uUqJGl/gyZKrjT6s8Tvm1f1qqtITuOIZki+WVldUpFrpsjuVeQrYWY4wA88A3Y0
                                            qXRUW0U3O3CPFvQ3EUJ+F3VMcEeAH9Z4NshDhAQ94rM9in91wRlk/+BwVCc60Cjk
                                            kcrYKMkNMl4mKolX8ATTsdursVmg8tqWe7rhISujXnXPIvC6HZOFnt7ojVE7rEM9
                                            Fhdohhimh/MNRosg3DSYwC+D2ED17XgJxGyyNpQE/KcyGp0zJlsWirxCT8cvg7xS
                                            2p1rs7Sw6a5Vdi9nosz96gVw3FrNoV4KOFR/CFwKQEdG7TuD3+EG4gzNSuSZs5nr
                                            SFWkV73TwGDWmqHLdc7snZJN61teN2Zt5RwFuqJlkrqawWQEDwFKwtBykbEypA95
                                            W8sQCZWrgc3rpkfElo6wVJZoHffISPosC9NlDFNIAhKmPetiym4=
                                            =Hplb
                                            -----END PGP PRIVATE KEY BLOCK-----";
             var privateKeyBytes = Encoding.ASCII.GetBytes(privateKey);

            Stream keyStream = new MemoryStream(privateKeyBytes);
           

            char[] pgpPassPhrase = { 'A', 'n', 'P', 'o', 's', 't' };

            ASCIIEncoding ascii = new ASCIIEncoding();

            // Create an ASCII byte array.
            Byte[] bytes = ascii.GetBytes(privateKey);

            // Decode the bytes and display the resulting Unicode string.
            String decoded = ascii.GetString(bytes);

            //var pKey = asciiEncoding.GetBytes(privateKey);
            //var encrypted = decryptString.Encrypt("test 1");

            //var decrypted = decryptString.Decrypt(input, privateKeyBytes);
            string defaultFileName = @"C:\Users\544396\source\repos\AnPostPGPCertificate_file.txt";
            //Pgp.DecryptFile(messageStream, keyStream, pgpPassPhrase, defaultFileName);
            var decryptedData = Pgp.Decrypt(messageKeyBytes, keyStream, "AnPost");
            var stringData = Encoding.ASCII.GetString(decryptedData);
          
        }

    }
}

